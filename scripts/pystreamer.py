#!/usr/bin/env python
from __future__ import print_function
import cv2
import Queue as queue
import threading
import time
import roslib
roslib.load_manifest('cv_streamer')
import rospy
import sys
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import signal

# bufferless VideoCapture
class VideoCapture:
    def __init__(self, name):
        self.cap = cv2.VideoCapture(name)
        self.q = queue.Queue()
        t = threading.Thread(target=self._reader)
        t.daemon = True
        t.start()

    # read frames as soon as they are available, keeping only most recent one
    def _reader(self):
        while True:
            ret, frame = self.cap.read()
            if not ret:
                break
            if not self.q.empty():
                try:
                    self.q.get_nowait()   # discard previous (unprocessed) frame
                except queue.Empty:
                    pass
            self.q.put(frame)
            time.sleep(0.01)

    def read(self):
        return self.q.get()

def keyboardInterruptHandler(signal, frame):
    print("KeyboardInterrupt (ID: {}) has been caught. Cleaning up...".format(signal))
    cv2.destroyAllWindows()
    exit(0)

def main(args):
    rospy.init_node('Pystreamer', anonymous=True)
    stream_url = rospy.get_param('~url', 'http://10.42.0.57:8090/?action=stream')
    width = rospy.get_param('~width', 640)
    framerate = rospy.get_param('~framerate', 30)
    topic = rospy.get_param('~topic', '/camera/image_raw')
    rospy.loginfo("reading from {}".format(stream_url))
    cap = VideoCapture(stream_url)
    image_pub = rospy.Publisher(topic, Image, queue_size=1)
    rate = rospy.Rate(framerate) #hz
    bridge = CvBridge()
    while not rospy.is_shutdown():
        frame = cap.read()
        height = int(frame.shape[0] * width / frame.shape[1])
        dim = (width, height)
        resized = cv2.resize(frame, dim, interpolation = cv2.INTER_AREA)
        try:
            img = bridge.cv2_to_imgmsg(resized, "bgr8")
            image_pub.publish(img)
        except CvBridgeError as e:
            print(e)
        except KeyboardInterrupt:
            break
        rate.sleep()
    cv2.destroyAllWindows()
    rospy.spin()


if __name__ == '__main__':
    signal.signal(signal.SIGINT, keyboardInterruptHandler)
    main(sys.argv)
